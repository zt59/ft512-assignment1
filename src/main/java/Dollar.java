public class Dollar {
    //ch1
    Dollar(int amount) {
    }
    void times(int multiplier) {
    }
    int amount;
    int amount= 10;
    int amount= 5 * 2;
    void times(int multiplier) {
        amount= 5 * 2;
    }
    Dollar(int amount) {
        this.amount= amount;
    }
    void times(int multiplier) {
        amount= amount * 2;
    }
    void times(int multiplier) {
        amount= amount * multiplier;
    }
    void times(int multiplier) {
        amount *= multiplier;
    }

    //ch2
    Dollar times(int multiplier) {
        amount *= multiplier;
        return null;
    }
    Dollar times(int multiplier) {
        return new Dollar(amount * multiplier);
    }

    //ch3
    public boolean equals(Object object) {
        Dollar dollar= (Dollar) object;
        return amount == dollar.amount;
    }

    //ch4
    private int amount;
    Dollar times(int multiplier) {
        return new Dollar(amount * multiplier);
    }
    Money times(int multiplier) {
        return new Dollar(amount * multiplier);
    }
    static Dollar dollar(int amount) {
        return new Dollar(amount);
    }
    Money times(int multiplier) {
        return new Dollar(amount * multiplier, "USD");
    }

    Money times(int multiplier) {
        return new Dollar(amount * multiplier, currency);
    }
    Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
    static Money dollar(int amount) {
        return new Money(amount, "USD");
    }

}

//ch5
class Franc {
    private int amount;
    Franc(int amount) {
        this.amount= amount;
    }
    Franc times(int multiplier) {
        return new Franc(amount * multiplier);
    }

    public boolean equals(Object object) {
        Franc franc= (Franc) object;
        return amount == franc.amount;
    }
    Money times(int multiplier) {
        return new Franc(amount * multiplier, currency);
    }
    Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
    Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
}

//ch6
class Money{protected int amount;
    Money times(int amount) {
        return null;
        public String toString() {
            return amount + " " + currency;
        }
        public boolean equals(Object object) {
            Money money = (Money) object;
            return amount == money.amount
                    && getClass().equals(money.getClass());
        }

    }
    public boolean equals(Object object) {
        Money money = (Money) object;
        return amount == money.amount
                && currency().equals(money.currency());
    }
    Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
    Money plus(Money addend) {
        return new Money(amount + addend.amount, currency);
    }
    public Money reduce(String to) {
        int rate = (currency.equals("CHF") && to.equals("USD"))
                ? 2
                : 1;
        return new Money(amount / rate, to);
    }
    public Money reduce(Bank bank, String to) {
        int rate = (currency.equals("CHF") && to.equals("USD"))
                ? 2
                : 1;
        return new Money(amount / rate, to);
    }
    Expression plus(Expression addend) {
        return new Sum(this, addend);
    }
    Expression times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }
    public Expression times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }



}
class Dollar extends Money {
    private int amount;
    public boolean equals(Object object) {
        Money dollar= (Dollar) object;
        return amount == dollar.amount;
    }
    public boolean equals(Object object) {
        Money dollar= (Money) object;
        return amount == dollar.amount;
    }
    public boolean equals(Object object) {
        Money money= (Money) object;
        return amount == money. amount;
    }
    public boolean equals(Object object) {
        Money money= (Money) object;
        return amount == money.amount;
    }
    public Money reduce(Bank bank, String to) {
        int rate = bank.rate(currency, to);
        return new Money(amount / rate, to);
    }



}
class Franc extends Money {
    private int amount;
}
class Franc extends Money {
    public boolean equals(Object object) {
        Money franc= (Franc) object;
        return amount == franc.amount;
    }
    public boolean equals(Object object) {
        Money franc= (Money) object;
        return amount == franc.amount;
    }
    public boolean equals(Object object) {
        Money money= (Money) object;
        return amount == money.amount;
    }
    public boolean equals(Object object) {
        Money money = (Money) object;
        return amount == money.amount
                && getClass().equals(money.getClass());
    }
    Franc times(int multiplier) {
        return new Franc(amount * multiplier);
    }
    Money times(int multiplier) {
        return new Franc(amount * multiplier);
    }
    Money times(int multiplier) {
        return new Franc(amount * multiplier, "CHF");
    }
    Money times(int multiplier) {
        return new Franc(amount * multiplier, currency);
    }
    static Money franc(int amount){
        return new Money(amount, "CHF");
    }
    Expression plus(Money addend) {
        return new Sum(this, addend);
    }

}

abstract class Money
        abstract Money times(int multiplier);
        static Money dollar(int amount) {
            return new Dollar(amount);
        }
        static Money franc(int amount) {
            return new Franc(amount);
        }
        public void testCurrency() {
            assertEquals("USD", Money.dollar(1).currency());
            assertEquals("CHF", Money.franc(1).currency());
        }
        abstract String currency();
        String currency() {
            return "CHF";
        }
        String currency() {
            return "USD";
        }

private String currency;
        Franc(int amount) {
        this.amount = amount;
        currency = "CHF";
        }
        String currency() {
        return currency;
        }
private String currency;
        Dollar(int amount) {
        this.amount = amount;
        currency = "USD";
        }
        String currency() {
        return currency;
        }
        protected String currency;
        String currency() {
            return currency;
        }
        Franc(int amount, String currency) {
            this.amount = amount;
            this.currency = "CHF";
        }
        static Money franc(int amount) {
            return new Franc(amount, null);
        }
        Money times(int multiplier) {
            return new Franc(amount * multiplier, null);
        }
        Money times(int multiplier) {
            return Money.franc(amount * multiplier);
        }
        static Money franc(int amount) {
            return new Franc(amount, "CHF");
        }
        Franc(int amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }
        static Money dollar(int amount) {
            return new Dollar(amount, "USD");
        }
        Dollar(int amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }
        Money times(int multiplier) {
            return Money.dollar(amount * multiplier);
        }
        Money(int amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }
        Franc(int amount, String currency) {
            super(amount, currency);
        }

        Dollar(int amount, String currency) {
            super(amount, currency);
        }
        Money times(int multiplier) {
            return Money.franc(amount * multiplier);
        }
        Money times(int multiplier) {
            return Money.dollar(amount * multiplier);
        }
        Money times(int multiplier) {
            return Money.dollar(amount * multiplier);
        }

interface Expression
			Expression plus(Money addend) {
        return new Money(amount + addend.amount, currency);
        }
        Expression plus(Expression addend);
        Expression times(int multiplier);

class Money implements Expression
class Bank {
    Money reduce(Expression source, String to) {
        return null;
    }

    Money reduce(Expression source, String to) {
        return Money.dollar(10);
    }
    Money reduce(Expression source, String to) {
            return Money.dollar(10);
        }
    Money reduce(Expression source, String to) {
        Sum sum= (Sum) source;
        int amount= sum.augend.amount + sum.addend.amount;
        return new Money(amount, to);
    }
    Money reduce(Expression source, String to) {
        Sum sum= (Sum) source;
        return sum.reduce(to);
    }
    Money reduce(Expression source, String to) {
        if (source instanceof Money) return (Money) source;
        Sum sum= (Sum) source;
        return sum.reduce(to);
    }
    Money reduce(Expression source, String to) {
        if (source instanceof Money)
            return (Money) source.reduce(to);
        Sum sum= (Sum) source;
        return sum.reduce(to);
    }
    public Money reduce(String to) {
        return this;
    }
    Money reduce(Expression source, String to) {
        return source.reduce(to);
    }

    Money reduce(Expression source, String to) {
        return source.reduce(this, to);
    }
    int rate(String from, String to) {
        return (from.equals("CHF") && to.equals("USD"))
                ? 2
                : 1;
    }
    private Hashtable rates= new Hashtable();
    void addRate(String from, String to, int rate) {
        rates.put(new Pair(from, to), new Integer(rate));
    }
    int rate(String from, String to) {
        Integer rate= (Integer) rates.get(new Pair(from, to));
        return rate.intValue();
    }
    int rate(String from, String to) {
        if (from.equals(to)) return 1;
        Integer rate= (Integer) rates.get(new Pair(from, to));
        return rate.intValue();
    }



}
class Sum {
    Money augend;
    Money addend;
    Sum(Money augend, Money addend) {
    }
    Sum(Money augend, Money addend) {
        this.augend= augend;
        this.addend= addend;
    }
    public Money reduce(String to) {
        int amount= augend.amount + addend.amount;
        return new Money(amount, to);
    }
    public Money reduce(Bank bank, String to) {
        int amount= augend.amount + addend.amount;
        return new Money(amount, to);
    }
    public Money reduce(Bank bank, String to) {
        int amount= augend.amount + addend.amount;
        return new Money(amount, to);
    }
    public Money reduce(Bank bank, String to) {
        int amount= augend.reduce(bank, to).amount
                + addend.reduce(bank, to).amount;
        return new Money(amount, to);
    }
    Expression augend;
    Expression addend;
    Sum(Expression augend, Expression addend) {
        this.augend= augend;
        this.addend= addend;
    }
    public Expression plus(Expression addend) {
        return null;
    }
    public Expression times(int multiplier) {
        return new Sum(augend.times(multiplier),addend.times(multiplier));
    }



}
class Sum implements Expression
Money reduce(String to);
private class Pair {
    private String from;
    private String to;
    Pair(String from, String to) {
        this.from= from;
        this.to= to;

    }
    public boolean equals(Object object) {
        Pair pair= (Pair) object;
        return from.equals(pair.from) && to.equals(pair.to);
    }

    public int hashCode() {
        return 0;
    }
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }
    Expression times(int multiplier) {
        return new Sum(augend.times(multiplier),addend.times(multiplier));
    }



}
